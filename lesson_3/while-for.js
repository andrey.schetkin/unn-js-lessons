/**
 * Циклы while и for
 * Решите эти задачи сначала через цикл while, а затем через цикл for.
 */

// 1 - Выведите столбец чисел от 1 до 100.
let c1 = 1;
while(c1 <= 100) {
    console.log(c1);
    c1++;
}

for (let c = 1; c <= 100; c++) {
    console.log(c);    
}

// 2 - Выведите столбец чисел от 11 до 33.
let c2 = 11;
while (c2 <= 33) {
    console.log(c1);
    c2++;
}

for (let c = 11; c <= 33; c++) {
    console.log(c);
}

// 3 - Выведите столбец четных чисел в промежутке от 0 до 100.
let c3 = 0;
while (c3 <= 100) {
    if (c3 % 2 === 0)
        console.log(c3);
    c3++;
}

for (let c = 0; c <= 100; c++) {
    if (c % 2 === 0)
        console.log(c);
}

// 4 - С помощью цикла найдите сумму чисел от 1 до 100.
let sum1 = 0;
let c4 = 1;
while (c4 <= 100) {
    sum1 += c4;
    c4++;
}
console.log(sum1);

let sum2 = 0;
for (let c = 1; c <= 100; c++) {
    sum2 += c;
}
console.log(sum2);


/**
 * Работа с for для массивов
 */
// 1 - Дан массив с элементами [1, 2, 3, 4, 5].С помощью цикла for выведите все эти элементы на экран.
const arr1 = [1, 2, 3, 4, 5];
for (let index = 0; index < arr1.length; index++) {
    console.log(arr1[index]);    
}
// 2 - Дан массив с элементами [1, 2, 3, 4, 5].С помощью цикла for найдите сумму элементов этого массива.Запишите ее в переменную result.
const arr2 = [1, 2, 3, 4, 5];
let result = 0;
for (let index = 0; index < arr2.length; index++) {
    result += arr2[index];
}
console.log(result);


/**
 * Задачи общие.
 */

// 1 - Дан массив с элементами 2, 5, 9, 15, 0, 4. С помощью цикла for и оператора if выведите на экран столбец тех элементов массива, которые больше 3 - х, но меньше 10.
const arr3 = [2, 5, 9, 15, 0, 4];
for (const num of arr3) {
    if (num > 3 && num < 10)
        console.log(num);
}

// 2 - Дан массив с числами.Числа могут быть положительными и отрицательными.Найдите сумму положительных элементов массива.
const arr4 = [2, -5, 9, -15, 0, 4];
for (const num of arr4) {
    if (num > 3 && num < 10)
        console.log(num);
}

// 3 - Дан массив с элементами 1, 2, 5, 9, 4, 13, 4, 10. С помощью цикла for и оператора if проверьте есть ли в массиве элемент со значением, равным 4. Если есть - выведите на экран 'Есть!' и выйдите из цикла.Если нет - ничего делать не надо.
const arr5 = [1, 2, 5, 9, 4, 13, 4, 10];
for (const num of arr5) {
    if (num == 4){
        console.log("Есть!");
        break;
    }        
}

// 4 - Дан массив числами, например: [10, 20, 30, 50, 235, 3000].Выведите на экран только те числа из массива, которые начинаются на цифру 1, 2 или 5.
const arr6 = [10, 20, 30, 50, 235, 3000];
const foundedNums = [1, 2, 5];
for (const num of arr6) {
    if (foundedNums.includes(Number(String(num)[0]))) {
        console.log(num);
    }
}

// 5 - Дан массив с элементами 1, 2, 3, 4, 5, 6, 7, 8, 9. С помощью цикла for создайте строку '-1-2-3-4-5-6-7-8-9-'.
const arr7 = [1, 2, 3, 4, 5, 6, 7, 8, 9];
let str7 = "-";
for (const num of arr7) {
    str7 += num + "-";
}
console.log(str7);

// 6 - Составьте массив дней недели.С помощью цикла for выведите все дни недели, а выходные дни выведите жирным.
const week = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
];
for (const day of week) {
    if (day[0] == "S") {
        console.log(day.bold()); // жирным будет только в браузере, если вставить как html, или document.write
    } else {
        console.log(day);
    }
}

// 7 - Составьте массив дней недели.С помощью цикла for выведите все дни недели, а текущий день выведите курсивом.Текущий день должен храниться в переменной day.
const week2 = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
];
const day = "Friday";
for (const dayOfWeek of week2) {
    if (dayOfWeek == day) {
        console.log(dayOfWeek.italics()); // жирным будет только в браузере, если вставить как html, или document.write
    } else {
        console.log(dayOfWeek);
    }
}

// 8 - Дано число n = 1000. Делите его на 2 столько раз, пока результат деления не станет меньше 50. Какое число получится ? Посчитайте количество итераций, необходимых для этого(итерация - это проход цикла), и запишите его в переменную num.
let n = 1000;
let num = 0;
do {
    n /= 2;
    console.log(n);
    num++;
} while (n >= 50);