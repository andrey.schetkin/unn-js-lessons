"use strict";

// 1 - Используя метод map() напишите код, который получает из массива строк новый массив, содержащий их длины.
const vegetables = ['Капуста', 'Репа', 'Редиска', 'Морковка'];
const vegetableLength = vegetables.map(item => item.length);
console.log(1, vegetableLength); // 7,4,7,8


// 2 - Имеется массив простых чисел: numbers = [2, 3, 5, 7, 11, 13, 17, 19]. Использую метод reduce() напишите функцию currentSums(numbers), которая возвращает новый массив из такого же числа элементов, в котором на каждой позиции будет находиться сумма элементов массива numbers до этой позиции включительно.

let numbers = [2, 3, 5, 7, 11, 13, 17];
function currentSums(arr) {
    let result = [];
    let totalSum = arr.reduce((acc, cur) => {
        result.push(acc);
        return acc + cur;
    });
    result.push(totalSum);
    return result;
}

console.log(2, currentSums(numbers));


// 3 - Напишите код, который получает из массива чисел новый массив, содержащий пары чисел, которые в сумме должны быть равны семи: (0:7), (1:6) и т.д.

const arr3 = [0, 1, 2, 3, 4, 5, 6, 7];

function sumSeven(numbers) {
    let resultArr = [];
    let index;
    while (numbers.length) {
        const num = numbers.pop();
        const num2 = 7 - num;
        if ((index = numbers.indexOf(num2)) > -1) {
            resultArr.push([num, num2]);
            numbers.splice(index, 1);
        }
    }
    return resultArr;
}

console.log(3, sumSeven(arr3));


// 4 - Перед вами переменная, содержащая строку. Напишите код, создащий массив, который будет состоять из первых букв слов строки str. 

const str4 = "Каждый охотник желает знать, где сидит фазан.";
console.log(4, str4.split(" ").map(word => word[0]));  // [К,о,ж,з,г,с,ф]


// 5 - Перед вами переменная, содержащая строку.Напишите код, создащий массив, который будет состоять из строк, состоящих из предыдущего, текущего и следующего символа строки str.

const str5 = "JavaScript";
let result5 = [];
[...str5].map((char, index) => {
    let start = index ? index - 1 : 0;
    result5.push(str5.slice(start, index + 2));
});

console.log(5, result5); // [Ja,Jav,ava,vaS,aSc,Scr,cri,rip,ipt,pt]

// 6 - Напишите код, преобразующий массив цифр, которые располагаются неупорядоченно, в массив цифр расположенных по убыванию их значений.

var numerics = [5, 7, 2, 9, 3, 1, 8];
console.log(6, numerics.sort((a, b) => b - a)); // [9,8,7,5,3,2,1]


// 7 - Напишите код, объединяющий три массива цифр, и располагающий цифры, в полученном массиве, в порядке убывания их значений через пробел.

const a7 = [1, 2, 3];
const b7 = [4, 5, 6];
const c7 = [7, 8, 9];

function getArr(a, b, c) {
    return a.concat(b, c).sort((a, b) => b - a).join(" ");
}

console.log(7, getArr(a7, b7, c7)); // [9 8 7 6 5 4 3 2 1]


// 8 - Дан двухмерный массив с числами, например[[1, 2, 3], [4, 5], [6]].Найдите сумму элементов этого массива.Массив, конечно же, может быть произвольным.Показать решение.
function sumOfArrs(arrs) {
    let result = 0;
    arrs.map(arr => {
        arr.forEach(num => result += num);
    });
    return result;
}

console.log(8, sumOfArrs([[1, 2, 3], [4, 5], [6]]));


// 9 - Дан трехмерный массив с числами, например[[[1, 2], [3, 4]], [[5, 6], [7, 8]]].Найдите сумму элементов этого массива.Массив, конечно же, может быть произвольным.
function sumOftripleArrs(arrs) {
    let result = 0;
    arrs.map(arr => {result += sumOfArrs(arr)});
    return result;
}

console.log(9, sumOftripleArrs([[[1, 2], [3, 4]], [[5, 6], [7, 8]]]));


// 10 - Дан массив с числами.Не используя метода reverse переверните его элементы в обратном порядке.
const arr10 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
let result10 = [];
while (arr10.length) {
    result10.push(arr10.pop());
}
console.log(10, result10);


// 11 - Дан массив с числами.Узнайте сколько элементов с начала массива надо сложить, чтобы в сумме получилось больше 10 - ти.
const arr11 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
let counter11 = 0;
let tmp11 = 0;
for (let num of arr11) {
    tmp11 += num;
    counter11++;
    if (tmp11 > 10) {
        break;
    }
}
console.log(11, counter11);


// 12 - Сделайте функцию arrayFill, которая будет заполнять массив заданными значениями.Первым параметром функция принимает значение, которым заполнять массив, а вторым - сколько элементов должно быть в массиве.Пример: arrayFill('x', 5) сделает массив['x', 'x', 'x', 'x', 'x'].

function arrayFill(char, cnt) {
    return [...char.repeat(cnt)];
}
console.log(12, arrayFill('x', 5));