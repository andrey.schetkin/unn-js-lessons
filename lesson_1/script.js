"use strict";

// 1
console.log("Hello World!");

// 2
const number = 123;
const string = "hello";
const boolean = true;
const nully = null;
const und = undefined;
const obj = {};
const symbol = Symbol();

// 3
console.log(number);
console.log(string);
console.log(boolean);
console.log(nully);
console.log(und);
console.log(obj);
console.log(symbol);

// 4
console.log(typeof number);
console.log(typeof string);
console.log(typeof boolean);
console.log(typeof nully);
console.log(typeof und);
console.log(typeof obj);
console.log(typeof symbol);

// 5
// TypeError: Assignment to constant variable.
//number = 456;

// 6
// TypeError: Assignment to constant variable.
//obj = {"key": "value"};

// 7
let obj2 = {};
obj2 = { "key": "value"};

// 8
var obj3 = {};
obj3 = { "key": "value"};