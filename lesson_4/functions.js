// 1 - Сделайте функцию, которая возвращает квадрат числа. Число передается параметром.
function square(num) {
    return num * num;
}

// 3 - Сделайте функцию, которая возвращает сумму двух чисел.
function sum(num1, num2) {
    return num1 + num2;
}

// 5 - Сделайте функцию, которая отнимает от первого числа второе и делит на третье.
function fifth(num1, num2, num3) {
    return (num1 - num2) / num3;
}

// 7 - Сделайте функцию, которая принимает параметром число от 1 до 7, а возвращает день недели на русском языке.
function seventh(day) {
    const week = [
        "Понедельник",
        "Вторник",
        "Среда",
        "Четверг",
        "Пятница",
        "Суббота",
        "Воскресенье",
    ];
    return week[day-1];
}

// 8 - Сделайте функцию, которая параметрами принимает 2 числа. Если эти числа равны - пусть функция вернет true, а если не равны - false.
function eighth(num1, num2) {
    return num1 == num2;
}

// 10 - Сделайте функцию, которая параметрами принимает 2 числа. Если их сумма больше 10 - пусть функция вернет true, а если нет - false.
function tenth(num1, num2) {
    return num1 + num2 > 10;
}

// 12 - Сделайте функцию, которая параметром принимает число и проверяет - отрицательное оно или нет. Если отрицательное - пусть функция вернет true, а если нет - false.
function twelfth(num) {
    return num < 0;
}

// 13 - Сделайте функцию isNumberInRange, которая параметром принимает число и проверяет, что оно больше нуля и меньше 10. Если это так - пусть функция возвращает true, если не так - false.
function isNumberInRange(num) {
    return num > 0 && num < 10;
}

// 14 - Дан массив с числами. Запишите в новый массив только те числа, которые больше нуля и меньше 10-ти. Для этого используйте вспомогательную функцию isNumberInRange из предыдущей задачи.
function fourteenth(arr) {
    let newArr = [];
    for(let num of arr) {
        if (isNumberInRange(num)) {
            newArr.push(num);
        }
    }
    return newArr;
}

// 15 - Сделайте функцию getDigitsSum (digit - это цифра), которая параметром принимает целое число и возвращает сумму его цифр.
function getDigitsSum(num) {
    let digitsSum = 0;
    for (let digit of String(num)) {
        digitsSum += Number(digit);
    }
    return digitsSum;
}

// 16 - Найдите все года от 1 до 2020, сумма цифр которых равна 13. Для этого используйте вспомогательную функцию getDigitsSum из предыдущей задачи.
let fndYears = []; // здесь будут все года, кратные 13
for (let year = 1; year <= 2020; year++) {
    if (getDigitsSum(year) == 13) {
        fndYears.push(year);
    }
}

// 17 - Сделайте функцию isEven() (even - это четный), которая параметром принимает целое число и проверяет: четное оно или нет. Если четное - пусть функция возвращает true, если нечетное - false.
function isEven(num) {
    return num % 2 == 0;
}

// 18 - Дан массив с целыми числами. Создайте из него новый массив, где останутся лежать только четные из этих чисел. Для этого используйте вспомогательную функцию isEven из предыдущей задачи.
let arrOfNumbers = [1, 2, 3, 4, 5, 6, 20, 75];
function getEvenNums(arr) {
    let newArr = [];
    for (const num of arr) {
        if (isEven(num)) {
            newArr.push(num);
        }
    }
    return newArr;
}
arrOfNumbers = getEvenNums(arrOfNumbers);

// 19 - Сделайте функцию getDivisors, которая параметром принимает число и возвращает массив его делителей (чисел, на которое делится данное число).
function getDivisors(num) {
    let divisors = [];
    for (let n = 1; n <= num; n++) {
        if (num % n == 0) {
            divisors.push(n);
        }
    }
    return divisors;
}

// 20 - Дан массив с числами. Выведите последовательно его элементы используя рекурсию и не используя цикл.
const arrOfNumbers2 = [2, 7, 56, 23];
function printArrayEleems(arr) {
    console.log(arr.shift());
    if (arr.length)
        printArrayEleems(arr);
}
// printArrayEleems(arrOfNumbers2);

// 21 - Дано число. Сложите его цифры. Если сумма получилась более 9-ти, опять сложите его цифры. И так, пока сумма не станет однозначным числом (9 и менее).
const num21 = 567;
function twentyFirst(num) {
    num = getDigitsSum(num);
    if (num > 9){
        return twentyFirst(num);
    }
    return num;
}
// console.log(twentyFirst(num21));