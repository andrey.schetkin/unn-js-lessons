"use strict";

// 1 - Преобразовать строку в массив слов
// Напишите функцию stringToarray(str), которая преобразует строку в массив слов.
const str = 'Каждый охотник желает знать';

function stringToarray(str) {
    return str.split(" ");
};

console.log(1, stringToarray(str));


// 2 - Удаление указанного количества символов из строки
// Напишите функцию delete_characters(str, length), которая возвращает подстроку, состоящую из указанного количества символов.
const str2 = 'Каждый охотник желает знать';

function delete_characters(str, length) {
    return str.slice(0, length);
};

console.log(2, delete_characters(str2, 3));


// 3 - Вставить тире между словами строки
// Напишите функцию insert_dash(str), которая принимает строку str в качестве аргумента и вставляет тире (-) между словами. При этом все символы строки необходимо перевести в верхний регистр.
const str3 = "HTML JavaScript PHP";

function insert_dash(str) {
    return str.replace(/\s/g, "-").toUpperCase();
}

console.log(3, insert_dash(str3));


// 4. Сделать первую букву строки прописной
// Напишите функцию, которая принимает строку в качестве аргумента и преобразует регистр первого символа строки из нижнего регистра в верхний.
const str4 = "string not starting with capital";

function cursive_letter(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

console.log(4, cursive_letter(str4));


// 5. Первая буква каждого слова заглавная
// Напишите функцию capitalize(str), которая возвращает строку, в которой каждое слово начинается с заглавной буквы.
const str5 = "каждый охотник желает знать";

function capitalize(str) {
    return stringToarray(str).map(val => cursive_letter(val)).join(" ");
}

console.log(5, capitalize(str5));


// 6. Смена регистра символов строки
// Напишите функцию change_register(str), которая принимает в качестве аргумента строку и и заменяет регистр каждого символа на противоположный.Например, если вводится «КаЖдЫй ОхОтНиК», то на выходе должно быть «кАжДыЙ оХоТнИк».
const str6 = "КаЖдЫй ОхОтНиК жЕлАеТ зНаТь";

function change_register(str) {
    let result = "";
    for (const char of str) {
        if (char === " ") {
            result += char;
            continue;
        }

        const upper = char.toUpperCase();
        if (char === upper) { // значит заглавная
            result += char.toLowerCase();
        } else {
            result += upper;
        }
    }

    return result;
}

console.log(6, change_register(str6));


// 7. Удалить все не буквенно-цифровые символы
// Напишите функцию remove_char(str), которая возвращает строку, очищенную от всех не буквенно - цифровых символов.
const str7 = "every., -/ hunter #! wishes ;: {} to $ % ^ & * know";

function remove_char(str) {
    return str.replace(/[^0-9A-Za-z]/g, "");
};

console.log(7, remove_char(str7));


// 8. Нулевое заполнение строки
// Напишите функцию zeros(num, len), которая дополняет нулями до указаной длины числовое значение с дополнительным знаком «+» или « -« в зависимости от передаваемого аргумента.
function zeros(num, len, sign) {
    let strNum = num.toString();
    if (len > strNum.length) {
        strNum += "0".repeat(len - strNum.length);
    }
    return sign + strNum;
};

console.log(8, zeros(72, 4, "-")); // -7200


// 9. Сравнение строк без учёта регистра
// Напишите функцию comparison(str1, str2), которая сравнивает строки без учёта регистра символов.

function comparison(str1, str2) {
    return str1.toLowerCase() === str2.toLowerCase();
};

console.log(9, comparison("сТрОкА", "СтРоКа"));


// 10. Поиск без учета регистра
// Напишите функцию insensitive_search(str1, str2), которая осуществляет поиск подстроки str2 в строкеstr1 без учёта регистра символов.

function insensitive_search(str1, str2) {
    return str1.toLowerCase().indexOf(str2.toLowerCase());
};

console.log(10, insensitive_search("сТрОкА", "оКа"));


// 11. ВерблюжийРегистр (CamelCase)
// Напишите функцию initCap(str), которая преобразует стиль написания составных слов строки в CamelCase, при котором несколько слов пишутся слитно без пробелов, при этом каждое слово внутри строки пишется с заглавной буквы.

const str11 = "hEllo woRld";

function initCap(str) {
    return stringToarray(str).map(
        val => val.charAt(0).toUpperCase() + val.slice(1).toLowerCase()
    ).join("");
};

console.log(11, initCap(str11));


// 12. Змеиный_регистр(snake_case)
// Напишите функцию initSnake(str), которая преобразует стиль написания составных слов строки из CamelCase в snake_case, при котором несколько слов разделяются символом подчеркивания(_), причём каждое слово пишется с маленькой буквы.

const str12 = "hEllo woRld";

function initSnake(str) {
    return stringToarray(str).map(
        val => val.toLowerCase()
    ).join("_");
};

console.log(12, initSnake(str12));


// 13. Повторить строку n раз
// Напишите функцию repeatStr(str, n), которая вовращает строку повторяемую определённое количество раз.

function repeatStr(str, n) { 
    return str.repeat(n);
};

console.log(13, repeatStr("повторить/", 3));


// 14. Получить имя файла
// Напишите функцию path(pathname), которая вовращает имя файла(подстрока после последнего символа "") из полного пути к файлу.

const pathname = "/home/user/dir/file.txt";

function path(pathname) {
    const pathArr = pathname.split("/");
    return pathArr[pathArr.length - 1];

};

console.log(14, path(pathname));


// 15. Заканчивается ли строка символами другой строки
// Создайте метод объекта String endsWith(), который сравнивает подстроку str1 с окончанием исходной строки str и определяет заканчивается ли строка символами подстроки.

const str15 = "Каждый охотник желает знать";
const str151 = "скрипт";
const str152 = "знать";

String.prototype.endsWith = function (substring) {
    return this.slice(-substring.length) === substring;
};
console.log(15.1, str15.endsWith(str151));
console.log(15.2, str15.endsWith(str152));


// 16. Подстрока до/после указанного символа
// Напишите функцию getSubstr(str, char, pos), которая возвращает часть строки, расположенную после или до указанного символа char в зависимости от параметра pos.

const str16 = 'Астрономия — Наука о небесных телах';

function getSubstr(str, char, pos) {
    const index = str.indexOf(char);
    if (index > -1) {
        if (pos >= 0)
            return str.slice(index + 1, index + pos + 1);
        return str.slice(index + pos, index);
    }
    return false;
};

console.log(16, getSubstr(str16, "и", 6));


// 17. Вставить подстроку в указанную позицию строки
// Напишите функцию insert(str, substr, pos), которая вставляет подстроку substr в указханную позицию pos строки str.По умолчанию подстрока вставляется в начало строки.

const str17 = "первое второе третье";

function insert(str, substr, pos = 0) {

    return str.slice(0, pos) + substr + str.slice(pos);

};

console.log(17, insert(str17, "слово ", 7));


// 18. Ограничить длину строки
// Напишите функцию limitStr(str, n, symb), которая обрезает строку, если она длиннее указанного количества символов n.Усеченная строка должна заканчиваться троеточием «...» (если не задан параметр symb) или заданным символом symb.

const str18 = "строка строка";

function limitStr(str, n, symb = "...") {
    if (str.length > n) {
        return str.slice(0, n) + symb;
    }
    return str;
};

console.log(18, limitStr(str18, 6));


// 19. Поделить строку на фрагменты
const str19 = "строка строка строка";
function cutString(str, n) {
    let resArr = [];
    while (str.length > n) {
        resArr.push(str.slice(0, n));
        str = str.slice(n);
    }
    resArr.push(str);
    return resArr;
};

console.log(19, cutString(str19, 6));


// 20. Количество вхождений символа в строке
// Напишите функцию count(str, stringsearch), которая возвращает количество символов stringsearch в строке str.

const symb20 = "о", str20 = "Астрономия это наука о небесных объектах";

function count(str, symb) {
    let cnt = 0;
    let pos = -1;
    while ((pos = str.indexOf(symb, pos + 1) )!= -1) {
        cnt += 1;
    }
    return cnt;
};

console.log(20, count(str20, symb20));


// 21. Удалить HTML-теги из строки
// Напишите функцию cutTegs(str), которая возвращает строку str, очищенную от всех HTML - тегов.

const str21 = `

Здесь <a href="#">важная</a> информация

о тегах HTML.` 

function cutTegs(str) {
    return str.replace(/(<.*?>)/g, "").replace(/[\r|\n]/gi, " ").replace(/\s{2,}/gi, " ").trim();
};

console.log(21, cutTegs(str21));


// 22. Удалить лишние пробелы из строки
// Напишите функцию strip(str), которая удаляет все лишние пробелы из строки str.

const str22 = "    Max is a good      boy     ";

function strip(str) {
    return str.replace(/\s{2,}/gi, " ").trim();
};

console.log(22, cutTegs(str22));


// 23. Удалить лишние слова из строки
// Напишите функцию cutString(str, n), которая удаляет лишние слова из строки str, оставив в ней n слов.

const str23 = "Сила тяжести приложена к центру масс тела";

function cutString2(str, n) {
    let pos = -1;
    while (n > 0 && (pos = str.indexOf(" ", pos + 1)) != -1) {
        n--;
    }
    return str.slice(0, pos < 0 ? str.length : pos);
};

console.log(23, cutString2(str23, 3));


// 24. Сортировка символов строки по алфавиту
// Напишите функцию alphabetize(str), которая возвращает строку, отсортировав её символы в алфавитном порядке.

const str24 = ';skdaijfn fdsgokm';

function alphabetize(str) {
    let charArr = cutString(str, 1);
    return charArr.sort().join("");
};

console.log(24, alphabetize(str24));


// 25. Найти слово в строке
// Напишите функцию findWord(word, str), которая проверяет, существует ли в строке str слова word.

const str25 = 'abc def ghi jkl mno pqr stu';

function findWord(word, str) {
    return str.indexOf(word) > -1;
};

console.log(25, findWord("ghi", str25));